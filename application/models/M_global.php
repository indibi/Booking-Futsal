<?php

Class M_global extends CI_Model {

    function confGet($get){
        $this->db->from('kel_web');
        $this->db->where('name', $get);
        $query = $this->db->get();
        return $query->row();
    }

    function favLap($id_user){
        return $this->db->query("SELECT id_user, lapangan, COUNT(lapangan) as total FROM book WHERE id_user = $id_user GROUP BY id_user, lapangan ORDER BY total DESC LIMIT 1")->row();
    }

    function custom($query){
        return $this->db->query($query)->result();
    }

    function Incustom($query){
        return $this->db->query($query);
    }

    function count($count,$table,$where){
        $this->db->select($count);
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function countAll($count,$table){
        $this->db->select($count);
        $this->db->from($table);
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }
    function insert($table, $data){
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    function update($table, $data, $where){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($table);
    }

    function check($table, $where){
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        if($query->num_rows() >= 1) {
            return false;
        } else {
            return true;
        }
    }

    function getOne($table, $where, $select = '*'){
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        $row = $query->row();
        return $row;
    }

    function get($table, $where, $select = '*'){
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($where);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getAll($table, $select = '*'){
        $this->db->select($select);
        $this->db->from($table);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function del($table,$where){
        $this->db->delete($table, $where);
    }

}
