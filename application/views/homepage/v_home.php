
        <!-- Background Image -->
		<div class="bg-img" style="background-image: url('<?php echo base_url("assets/homepage/img/background1.jpg");?>');">
			<div class="overlay"></div>
		</div>
		<!-- /Background Image -->
        <!-- home wrapper -->
		<div class="home-wrapper">
			<div class="container">
				<div class="row">

					<!-- home content -->
					<div class="col-md-10 col-md-offset-1">
						<div class="home-content">
							<h1 class="white-text">Welcome to AWK Futsal</h1>
							<p class="white-text">Tempat futsal terbaik pilihan anda di Bekasi.
							</p>
							<a href="<?php echo base_url('user/booking.html'); ?>" style="color:#fff"><button class="main-btn">Book Now !!</button></a>
						</div>
					</div>
					<!-- /home content -->

				</div>
			</div>
		</div>
		<!-- /home wrapper -->

	</header>
	<!-- /Header -->

	<!-- About -->
	<div id="about" class="section md-padding">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Section header -->
				<div class="section-header text-center">
					<h2 class="title"><?php echo $tentang->title; ?></h2>
				</div>
				<!-- /Section header -->
				<!-- about -->
				<div class="col-md-12">
					<?php echo $tentang->value; ?>
				</div>
				<!-- /about -->

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>
	<!-- /About -->

	<!-- Service -->
	<div id="service" class="section md-padding">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Section header -->
				<div class="section-header">
					<h2 class="title"><?php echo $fasilitas->title; ?></h2>
				</div>
                <!-- /Section header -->
                <?php echo $fasilitas->value; ?>

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>
	<!-- /Service -->


	<!-- Why Choose Us -->
	<div id="booking" class="section md-padding">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- why choose us content -->
				<div class="col-md-12">
					<div class="section-header">
                    <h2 class="title">Booking Lapangan <br><small>Cek jadwal lapangan yang tersedia di AWK Futsal.</small></h2>
                    <div class="col-md-6">
                        <select class="form-control" name="lap" id="lap" onChange="resetTgl();" required>
							<option value="" selected disabled>-- Pilih Lapangan --</option>
							<option value="1">Lapangan 1</option>
							<option value="2">Lapangan 2</option>
							<option value="3">Lapangan 3</option>
						</select>
                    </div>
                    <div class="col-md-6">
                        <input type="text" placeholder="Tanggal Main" class="form-control pull-right" name="tgl" id="tgl" onChange="getJam(this.value);" autocomplete="off" disabled required>
                    </div>
                    <br>
					</div>
					<table class="table table-hover" id="jam">
						<tr>
							<th style="text-align:center">Pilih lapangan dan tanggal terlebih dahulu</th>
						</tr>
                    </table>
					<a href="<?php echo base_url('user/booking.html'); ?>" style="color:#fff"><button class="main-btn btn-block">Book Now !!</button></a>
				</div>
				<!-- /why choose us content -->

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>
	<!-- /Why Choose Us -->

	<!-- Blog -->
	<div id="event" class="section md-padding bg-grey">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Section header -->
				<div class="section-header text-center">
					<h2 class="title">Event</h2>
				</div>
				<!-- /Section header -->

				<?php
				foreach($event as $row){
				?>
				<!-- blog -->
				<div class="col-md-4">
					<div class="blog">
						<div class="blog-content">
							<ul class="blog-meta">
								<li><i class="fa fa-user"></i>Admin</li>
								<li><i class="fa fa-clock-o"></i><?php echo date('d/m/Y', strtotime($row->tanggal)) ?></li>
							</ul>
							<h3><a href="<?php echo base_url("homepage/event/$row->id_event.html") ?>"><?php echo $row->judul; ?></a></h3>
							<?php 
							$string = (strlen($row->isi) > 120) ? substr($row->isi,0,117).'...' : $row->isi;
                    		echo $string; ?><br/>
							<a href="<?php echo base_url("homepage/event/$row->id_event.html") ?>">Read more</a>
						</div>
					</div>
				</div>
				<!-- /blog -->
				<?php } ?>

                <div class="col-md-12 text-center">
                    <a  href="<?php echo base_url('homepage/event.html'); ?>">Tampilkan Lebih Banyak</a> 
                </div>

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>
	<!-- /Blog -->

	<!-- Contact -->
	<div id="contact" class="section md-padding">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Section-header -->
				<div class="section-header text-center">
					<h2 class="title">Kontak AWK Futsal</h2>
				</div>
				<!-- /Section-header -->

				<!-- contact -->
				<div class="col-sm-6">
					<div class="contact">
						<i class="fa fa-phone"></i>
						<h3>Telepon</h3>
						<p><?php echo $kontak->value; ?></p>
					</div>
				</div>
				<!-- /contact -->

				<!-- contact -->
				<div class="col-sm-6">
					<div class="contact">
						<i class="fa fa-map-marker"></i>
						<h3>Alamat</h3>
						<p><?php echo $alamat->value; ?></p>
					</div>
				</div>
				<!-- /contact -->
			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>
	<!-- /Contact -->