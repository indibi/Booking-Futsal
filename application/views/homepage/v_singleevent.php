<div class="header-wrapper sm-padding bg-grey">
			<div class="container">
                <h2>Event AWK Futsal</h2>
                <ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url("homepage.html");?>">Home</a></li>
					<li class="breadcrumb-item"><a href="<?php echo base_url("homepage/event.html");?>">Event</a></li>
					<li class="breadcrumb-item active">Single Event</li>
				</ul>
			</div>
		</div>
		<!-- /header wrapper -->

	</header>
	<!-- /Header -->

	<!-- Blog -->
	<div id="blog" class="section md-padding">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Main -->
				<main id="main" class="col-md-12">
					<div class="blog">
						<div class="blog-content">
							<ul class="blog-meta">
								<li><i class="fa fa-user"></i>Admin</li>
								<li><i class="fa fa-clock-o"></i><?php echo date('d/m/Y', strtotime($event->tanggal)) ?></li>
							</ul>
							<h3><?php echo $event->judul; ?></h3>
                            <p><?php echo $event->isi; ?></p>
                            
						</div>
                    </div>
				</main>
				<!-- /Main -->

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>
	<!-- /Blog -->