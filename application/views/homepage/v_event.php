<div class="header-wrapper sm-padding bg-grey">
			<div class="container">
                <h2>Event AWK Futsal</h2>
                <ul class="breadcrumb">
					<li class="breadcrumb-item"><a href="<?php echo base_url('homepage.html'); ?>">Home</a></li>
					<li class="breadcrumb-item active">Event</li>
				</ul>
			</div>
		</div>
		<!-- /header wrapper -->

	</header>
	<!-- /Header -->

	<!-- Blog -->
	<div id="blog" class="section md-padding">

		<!-- Container -->
		<div class="container">

			<!-- Row -->
			<div class="row">

				<!-- Main -->
				<main id="main" class="col-md-12">
					<?php
					foreach($event as $row){
					?>
					<div class="blog">
						<div class="blog-content">
							<ul class="blog-meta">
								<li><i class="fa fa-user"></i>Admin</li>
								<li><i class="fa fa-clock-o"></i><?php echo date('d/m/Y', strtotime($row->tanggal)) ?></li>
							</ul>
							<h3><a href="<?php echo base_url("homepage/event/$row->id_event.html") ?>"><?php echo $row->judul; ?></a></h3>
                            <?php 
							$string = (strlen($row->isi) > 330) ? substr($row->isi,0,327).'...' : $row->isi;
                    		echo $string; ?><br/>
							<a href="<?php echo base_url("homepage/event/$row->id_event.html") ?>">Read more ..</a>
						</div>
                    </div>
					<hr>
					<?php }
					if ($this->input->get('page')){
						$page = $this->input->get('page');
					} else {
						$page = 1;
					} ?>
					<a href="<?php if($page <= 1){ echo '#'; } else { echo "?page=".($page - 1); } ?>"><< Prev</a>
					<a href="<?php if($page >= $total_pages){ echo '#'; } else { echo "?page=".($page + 1); } ?>" style="float:right">Next >></a>
				</main>
				<!-- /Main -->

			</div>
			<!-- /Row -->

		</div>
		<!-- /Container -->

	</div>
	<!-- /Blog -->