  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Made with <i class="fa fa-heart" style="color: red;"></i> for <i class="fa fa-mortar-board"></i> 
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url("assets/bower_components/jquery/dist/jquery.min.js");?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url("assets/bower_components/bootstrap/dist/js/bootstrap.min.js");?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url("assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js");?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url("assets/bower_components/fastclick/lib/fastclick.js");?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("assets/dist/js/adminlte.min.js");?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url("assets/dist/js/demo.js");?>"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url("assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js");?>"></script>
<script src="<?php echo base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/1.9.41/autoNumeric.min.js"></script>

<script>
  $(function () {
    //Date picker
    $('#tgl').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      startDate: 'today',
      endDate: '+7d'
    })
  })

  $(function () {
    $('#example2').DataTable({
      'responsive': true
    })
  })
  function cetak() {
    window.print();
  }
</script>

<script>
function getJam(val) {
    var lap = $("#lap").val();
    var tgl = val;
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('ajax/getJam'); ?>",
        data: {lap: lap, tgl: tgl},
        success: function(data){
            console.log("lol");
            $("#jam").html(data);
        }
    });
}

function getWkt(val) {
    var lap = $("#lap").val();
    var tgl = $("#tgl").val();
    var jam = val;
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('ajax/getWkt'); ?>",
        data: {lap: lap, tgl: tgl, jam: jam},
        success: function(data){
            console.log("lul");
            $("#wkt").html(data);
        }
    });
}

function resetAll() {
    $("#tgl").val('');
    $("#jam").html("<option value='' disabled selected>-- Pilih Jam --</option>");
    $("#wkt").html("<option value='' disabled selected>-- Pilih Durasi -- </option>");
}
function resetWkt() {
    $("#wkt").html("<option value='' disabled selected>-- Pilih Durasi -- </option>");
}

jQuery(function($) {
    $('#jumlah').autoNumeric('init');    
});
</script>
<script>  
  function checkPasswordMatch() { 
    var pass = document.getElementById("password");       
    var password = $("#newPass").val();
    var confirmPassword = $("#confPass").val();
    
    if (password != confirmPassword){    
        $("#notice").html("Sandi tidak sama!!");
        pass.setAttribute("class", "form-group has-error");
        $("#pc").prop('disabled', true);
    }else{
        $("#notice").html("Sandi cocok");
        pass.setAttribute("class", "form-group has-success");
        $("#pc").prop('disabled', false);
    }
}

$(document).ready(function () {
   $("#newPass, #confPass").keyup(checkPasswordMatch);
});
</script>

</body>
</html>
