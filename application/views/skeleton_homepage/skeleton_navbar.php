<body>
	<!-- Header -->
	<header id=<?php if(uri_string() == "homepage"){echo "home";}?>>
		
		<!-- Nav -->
		<nav id="nav" class="navbar <?php if(uri_string() == "homepage"){echo "nav-transparent";}?>">
			<div class="container">
				<div class="navbar-header">
					<div class="nav-collapse">
						<span></span>
					</div>
					<!-- /Collapse nav button -->
				</div>

				<!--  Main navigation  -->
				<ul class="main-nav nav navbar-nav navbar-right">
					<?php if(uri_string() == "homepage"){?>
					<li><a href="#home">Home</a></li>
					<li><a href="#about">Tentang</a></li>
					<li><a href="#service">Fasilitas</a></li>
					<li><a href="#booking">Booking Cek</a></li>
					<li><a href="#event">Event</a></li>
					<li><a href="#contact">Kontak</a></li>
					<li><a href="<?php echo base_url("user/panel.html");?>">Login</a></li>
					<?php }else{?>
					<li><a href="<?php echo base_url("homepage/#home");?>">Home</a></li>
					<li><a href="<?php echo base_url("homepage/#about");?>">Tentang</a></li>
					<li><a href="<?php echo base_url("homepage/#service");?>">Fasilitas</a></li>
					<li><a href="<?php echo base_url("homepage/#booking");?>">Booking Cek</a></li>
					<li class="active"><a href="<?php echo base_url("homepage/#event");?>">Event</a></li>
					<li><a href="<?php echo base_url("homepage/#contact");?>">Kontak</a></li>
					<li><a href="<?php echo base_url("user/panel.html");?>">Login</a></li>
					<?php }?>
				</ul>
				<!-- /Main navigation -->

			</div>
		</nav>