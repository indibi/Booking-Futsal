<!-- Footer -->
	<footer id="footer" class="sm-padding bg-dark">
		<!-- Container -->
		<div class="container">
			<!-- Row -->
			<div class="row">
				<div class="col-md-12">
					<!-- footer logo -->
					<div class="footer-logo">
						<h1>AWK Futsal</h1>
					</div>
					<!-- /footer logo -->
					<!-- footer follow -->
					<ul class="footer-follow">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-youtube"></i></a></li>
					</ul>
					<!-- /footer follow -->
					<!-- footer copyright -->
					<div class="footer-copyright">
						<p><strong>Made with <i class="fa fa-heart" style="color: red;"></i> for <i class="fa fa-mortar-board"></i> </p>
					</div>
					<!-- /footer copyright -->
				</div>
			</div>
			<!-- /Row -->
		</div>
		<!-- /Container -->
	</footer>
	<!-- /Footer -->

	<!-- Back to top -->
	<div id="back-to-top"></div>
	<!-- /Back to top -->

	<!-- Preloader -->
	<div id="preloader">
		<div class="preloader">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
	<!-- /Preloader -->

	<!-- jQuery Plugins -->
	<script type="text/javascript" src="<?php echo base_url("assets/homepage/js/jquery.min.js");?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/homepage/js/bootstrap.min.js");?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/homepage/js/owl.carousel.min.js");?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/homepage/js/jquery.magnific-popup.js");?>"></script>
	<script type="text/javascript" src="<?php echo base_url("assets/homepage/js/main.js");?>"></script>
	<!-- bootstrap datepicker -->
	<script src="<?php echo base_url("assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js");?>"></script>
	<script>
	$(function () {
		//Date picker
		$('#tgl').datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		startDate: 'today',
		endDate: '+7d'
		})
	})
	</script>

	<script>
	function getJam(val) {
		var lap = $("#lap").val();
		var tgl = val;
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('ajax/getJamHome'); ?>",
			data: {lap: lap, tgl: tgl},
			success: function(data){
				console.log("lol");
				$("#jam").html(data);
			}
		});
	}

	function resetTgl(){
		$("#tgl").val('');
		$("#tgl").prop('disabled', false);
		$("#jam").html('<tr><th style="text-align:center">Pilih lapangan dan tanggal terlebih dahulu</th></tr>');
	}
	</script>

</body>

</html>