  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Made with <i class="fa fa-heart" style="color: red;"></i> for <i class="fa fa-mortar-board"></i> 
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url("assets/bower_components/jquery/dist/jquery.min.js");?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url("assets/bower_components/bootstrap/dist/js/bootstrap.min.js");?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url("assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js");?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url("assets/bower_components/fastclick/lib/fastclick.js");?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url("assets/dist/js/adminlte.min.js");?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url("assets/dist/js/demo.js");?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url("assets/bower_components/datatables.net/js/jquery.dataTables.min.js");?>"></script>
<script src="<?php echo base_url("assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js");?>"></script>
<script src="https://cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>
<!-- CK Editor -->
<script src="<?php echo base_url("assets/bower_components/ckeditor/ckeditor.js");?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/autonumeric/1.9.41/autoNumeric.min.js"></script>

<?php
  $curr_url = uri_string();
  if ($curr_url == "admin/config") { ?>
  <script>
    $(function () {
      CKEDITOR.replace('about');
      CKEDITOR.replace('fasilitas');
      CKEDITOR.replace('peraturan');
    })
  </script>  
<?php } 

if ($curr_url == "admin/event" or $curr_url == "admin/edit_event") { ?>
  <script>
    $(function () {
        CKEDITOR.replace('editor1');
    })
  </script>  
<?php } ?>


<script>
  //datatable
  $(function () {
    $('#example1').DataTable({
      'responsive': true
    })
  })

  $(function () {
    $('#tpanel').DataTable({
      'order': [[ 0, 'desc' ]],
      'responsive': true
    })
  })
  
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })

  jQuery(function($) {
    $('.int').autoNumeric('init');    
  });
</script>
</body>
</html>
