<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manajemen Event
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Edit Event</h3> 
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
            <form action="" method="post">
                <div class="form-group">
                  <input type="hidden" name="ide" value="<?php echo $event->id_event; ?>">
                  <input type="text" name="judul" class="form-control" placeholder="Judul Event" value="<?php echo $event->judul; ?>" required>
                </div>
                <div class="form-group">
                    <textarea id="editor1" name="isi" rows="10" cols="80" required>
                        <?php echo $event->isi; ?>
                    </textarea>
                </div>    
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" name="upEv" value="true" class="btn btn-block btn-primary">Perbarui</button>
                </form>
                <a href="<?php echo base_url('admin/event.html'); ?>" class="btn btn-block btn-danger"> Batal</a>
            </div>
          </div>
          
          <!-- /.box -->
        </div>
      </div>
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
