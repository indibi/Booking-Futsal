<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kelola Website
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="col-sm-6">
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Harga & Kontak</h3>    
                </div>
                <div class="box-body ">
                    <form class="form-horizontal" action="" method="post">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Harga Lapangan</label>
                            <div class="col-sm-8">
                                <input type="text" name="harga" class="form-control int" value="<?php echo $harga->value ?>" placeholder="Harga" data-a-sign="Rp " data-a-dec="," data-a-sep="." required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Kontak AWK Futsal</label>
                            <div class="col-sm-8">
                                <input type="text" name="kontak" class="form-control" value="<?php echo $kontak->value ?>" placeholder="Kontak" required>
                            </div>
                        </div>
                        <div id="password" class="form-group">
                            <label class="col-sm-4 control-label">Alamat AWK Futsal</label>
                            <div class="col-sm-8">
                                <textarea name="alamat" class="form-control" placeholder="Alamat" rows="1" required><?php echo $alamat->value ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" name="chMain" value="true" class="btn btn-block btn-success">Ubah</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Edit Rekening Milik AWK Futsal</h3>    
                </div>
                <div class="box-body ">
                    <form class="form-horizontal" action="" method="post">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Bank</label>
                            <div class="col-sm-8">
                                <input type="text" name="bank" class="form-control" value="<?php echo $bank->value ?>" placeholder="Bank" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Rekening AWK Futsal</label>
                            <div class="col-sm-8">
                                <input type="text" name="rekening" class="form-control" value="<?php echo $rekening->value; ?>" placeholder="Rekening" required>
                            </div>
                        </div>
                        <div id="password" class="form-group">
                            <label class="col-sm-4 control-label">Atas Nama</label>
                            <div class="col-sm-8">
                                <input type="text" name="namaRek" class="form-control" value="<?php echo $nama_rek->value ?>" placeholder="Atas Nama" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" name="chBank" value="true" class="btn btn-block btn-success">Ubah</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-xs-12">
          <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Edit Tentang AWK Futsal</h3>    
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
            <form action="" method="post">
                <div class="form-group">
                    <textarea id="about" name="tentang" rows="10" cols="80"><?php echo $tentang->value; ?></textarea>
                </div>    
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" name="chTentang" value="true" class="btn btn-block btn-success">Ubah</button>
                </form>
            </div>
          </div>
          <!-- /.box -->
        </div>

        <div class="col-xs-6">
          <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Edit Fasilitas</h3>    
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
            <form action="" method="post">
                <div class="form-group">
                    <textarea id="fasilitas" name="fasilitas" rows="10" cols="80"><?php echo $fasilitas->value; ?></textarea>
                </div>    
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" name="chFasilitas" value="true" class="btn btn-block btn-success">Ubah</button>
                </form>
            </div>
          </div>
          <!-- /.box -->
        </div>

        <div class="col-xs-6">
          <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Edit Peraturan AWK Futsal</h3>    
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
            <form action="" method="post">
                <div class="form-group">
                    <textarea id="peraturan" name="peraturan" rows="10" cols="80"><?php echo $peraturan->value; ?></textarea>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" name="chPeraturan" value="true" class="btn btn-block btn-success">Ubah</button>
                </form>
            </div>
          </div>
          <!-- /.box -->
        </div>

      </div>
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
