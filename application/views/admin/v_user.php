  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Manajemen Pelanggan       
        <small>Atur dan lihat mengenai informasi pelanggan.</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-danger">
            <div class="box-header">              
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
                <table id="example1" class="table table-bordered table-striped dt-responsive" width="100%">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Pelanggan</th> 
                  <th>Email</th>
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 0;
                foreach($user as $row){
                $no++;
                ?>
                <tr>
                  <td><?php echo $no; ?></td>
                  <td><?php echo $row->nama; ?></td>
                  <td><?php echo $row->email; ?></td>
                  <td>
                    <!-- Modal -->
                    <div class="modal fade" id="modal<?php echo $no; ?>">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Detail</h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-md-12">
                                <table class="table table-striped">
                                  <tr>
                                    <td colspan="2" style="text-align:center"><h4>Detail Pelanggan</h4></td>
                                  </tr>
                                  <tr>
                                    <td style="white-space: nowrap; text-align:right">Nama : </td>
                                    <td><?php echo $row->nama; ?></td>
                                  </tr>
                                  <tr>
                                    <td style="white-space: nowrap; text-align:right">Email : </td>
                                    <td><?php echo $row->email; ?></td>
                                  </tr>
                                  <tr>
                                    <td style="white-space: nowrap; text-align:right">No. HP : </td>
                                    <td><?php echo $row->hp; ?></td>
                                  </tr>
                                  <tr>
                                    <td style="white-space: nowrap; text-align:right">Alamat : </td>
                                    <td><?php echo $row->alamat; ?></td>
                                  </tr>
                                  <tr>
                                    <td style="white-space: nowrap; text-align:right">Jumlah Booking : </td>
                                    <td><?php echo $row->jmlBook; ?></td>
                                  </tr>
                                  <tr>
                                    <td style="white-space: nowrap; text-align:right">Lapangan Favorite : </td>
                                    <td>Lapangan <?php echo $row->lapangan; ?></td>
                                  </tr>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal<?php echo $no; ?>"><i class="fa fa-check"></i> Detail</button>
                    <form style="display:inline" action="" method="post" onsubmit="return confirm('Yakin hapus pelanggan?');">
                      <input type="hidden" name="idu" value="<?php echo $row->id_user; ?>">
                      <button type="submit" name="delUser" value="delUser" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus</button>
                    </form>
                  </td>
                </tr>
                <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
