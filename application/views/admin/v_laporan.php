  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan     
        <small>Lihat laporan bulanan dan harian.</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-danger">
            <div class="box-header">              
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
                <table id="example1" class="table table-bordered table-striped dt-responsive" width="100%">
                <thead>
                <tr>
                  <th>No</th>
                  <th>
                  <?php
                  if($harian==true){
                    echo "Laporan Tanggal";
                  }else{
                    echo "Laporan Bulan";
                  }
                  ?>
                  </th>
                  <th>Pendapatan Kotor</th>
                  <th>Pendapatan Bersih</th>
                  <th>Total Selisih</th>
                  <?php if($harian==false){ echo '<th>Option</th>'; } ?>
                </tr>
                </thead>
                <tbody>
                <?php
                    $no = 0;
                    foreach($laporan as $row){
                    $no++;
                ?>
                <tr>
                    <td><?php echo $no; ?></td>
                    <td>
                        <?php
                        $date=date_create($row->date);
                        echo date_format($date,"d F Y");
                        ?>
                    </td>
                    <td style="color: blue"><?php echo $row->kotor; ?></td>
                    <td style="color: green"><?php echo $row->bersih; ?></td>
                    <td style="color: red"><?php echo $row->selisih; ?></td>
                    <?php if($harian==false){ ?>
                    <td>                    
                        <a class="btn btn-primary" href="<?php echo base_url("admin/laporan/$row->date.html"); ?>"><i class="fa fa-calendar"></i> Lihat Harian</a>
                    </td>
                    <?php } ?>
                </tr>
                <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
