<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kelola Event
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Tambah Event</h3>    
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
            <form action="" method="post">
                <div class="form-group">
                  <input name="judul" type="text" class="form-control" placeholder="Judul Event">
                </div>
                <div class="form-group">
                    <textarea id="editor1" name="isi" rows="10" cols="80"></textarea>
                </div> 
                <div class="form-group">   
                <button type="submit" name="addEv" value="true" class="btn btn-block btn-primary">Simpan</button>
                </div>
            </form>
            </div>
            
          </div>
          
          <!-- /.box -->
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">Daftar Event</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <table id="example1" class="table table-bordered table-striped dt-responsive" width="100%">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Judul</th> 
                  <th>Tgl Rilis</th> 
                  <th style="width:49%">Summary</th> 
                  <th>Option</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no=0;
                foreach($event as $row){ $no++;
                ?>
                <tr>
                  <td style="vertical-align: middle;"><?php echo $no; ?></td>
                  <td style="vertical-align: middle;"><?php echo $row->judul; ?></td>
                  <td style="vertical-align: middle;"><?php echo date('d/m/Y', strtotime($row->tanggal)); ?></td>
                  <td style="vertical-align: middle;">
                    <?php
                    $string = (strlen($row->isi) > 216) ? substr($row->isi,0,213).'...' : $row->isi;
                    echo $string;
                    ?>
                  </td>
                  <td style="vertical-align: middle;">
                    <form action="<?php echo base_url('admin/edit_event.html'); ?>" method="post">
                      <input type="hidden" name="ide" value="<?php echo $row->id_event; ?>">
                      <button type="submit" name="edit" value="edit" class="btn btn-sm btn-block btn-primary"><i class="fa fa-edit"></i> Edit Event</button>
                    </form>
                    <div style="padding-top:10px"></div>
                    <form action="" method="post" onsubmit="return confirm('Yakin hapus event?');">
                      <input type="hidden" name="ide" value="<?php echo $row->id_event; ?>">
                      <button type="submit" name="delEv" value="delEv" class="btn btn-sm btn-block btn-danger"><i class="fa fa-trash"></i> Hapus Event</button>
                    <div style="padding-top:10px"></div>
                    <a href="<?php echo base_url("homepage/event/$row->id_event.html"); ?>" target="_blank" class="btn btn-sm btn-block btn-success"><i class="fa fa-eye"></i> Lihat Event</a>
                    </form>
                  </td>
                </tr>
                <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
