<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Selamat Datang di admin panel AWK Futsal
        <small>Anda login sebagai <?php echo $this->session->userdata('NAMA'); ?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-6">
          <div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Cara Pemakaian Admin Panel</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui numquam aliquam similique optio! Veniam eos dolore, facere officiis illum aperiam a nostrum voluptatum! Maiores quos eveniet reprehenderit autem quae sed.</p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <div class="col-xs-6">
          
          <div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Peraturan AWK Futsal</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <?php echo $peraturan->value; ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Daftar Booking Lapangan</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <table id="tpanel" class="table table-bordered table-striped dt-responsive" width="100%">
                <thead>
                <tr>
                  <th>Invoice</th>
                  <th>Tanggal</th>
                  <th>Nama</th>
                  <th>Jumlah Lapangan</th>
                  <th>Biaya Total</th>
                  <th>Status Booking</th>
                  <th>Option</th>
                  <th class="none">Detail Pesanan</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($pesanan as $row){ ?>
                <tr>
                  <td style="vertical-align: middle;"> <?php echo '#'.$row->id_inv; ?></td>
                  <td style="vertical-align: middle;"><?php echo date('d/m/Y', strtotime($row->stamp)); ?></td>
                  <td style="vertical-align: middle;"><?php echo $row->nama; ?></td>
                  <td style="vertical-align: middle;"><?php echo $row->jlapangan." Lapangan"; ?></td>
                  <td style="vertical-align: middle;"><?php echo rp($row->harga); ?></td>
                  <td style="vertical-align: middle;">
                    <?php
                    $sisa = $row->harga - $row->dp;
                    $status = $row->status;
                    if($status == "Menunggu Konfirmasi"){
                      $color = "bg-yellow";
                      $disable = true;
                      $disableck = true;
                    }elseif($status == "Proses Konfirmasi"){
                      $color = "bg-blue";
                      $disable = false;
                      $disableck = false;
                    }elseif($status == "Konfirmasi Berhasil"){
                      $color = "bg-green";
                      $disable = true;
                      $disableck = false;
                    }elseif($status == "Konfirmasi Gagal"){
                      $color = "bg-red";
                      $disable = true;
                      $disableck = false;
                    }elseif($status == "Booking Dibatalkan"){
                      $color = "bg-black";
                      $disable = true;
                      $disableck = true;
                    }
                    elseif($status == "Booking Selesai"){
                      $color = "bg-teal";
                      $disable = true;
                      $disableck = false;
                    }
                    ?>
                    <small class="label <?php echo $color; ?>" style="display:inline-block;width:100%"><?php echo $status; ?></small>
                  </td>
                  <td style="vertical-align: middle;">
                    <button type="button" class="btn btn-sm btn-block btn-primary <?php if($disableck==true){echo 'disabled';} ?>" <?php if($disableck==true){echo 'disabled';} ?> data-toggle="modal" data-target="#modal<?php echo $row->id_inv; ?>">Lihat Bukti Pembayaran</i></button>
                    
                    <!-- Modal -->
                    <div class="modal fade" id="modal<?php echo $row->id_inv; ?>">
                      <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Bukti Pembayaran #<?php echo $row->id_inv; ?></h4>
                          </div>
                          <div class="modal-body">
                            <div class="row">
                              <div class="col-md-6">
                                <a href="<?php echo base_url("assets/upload/$row->foto_bukti");?>" target="_blank"><img src="<?php echo base_url("assets/upload/$row->foto_bukti");?>" class="img-responsive" alt="Bukti Permbayaran"></a>
                                <br/>**Jika gambar tidak jelas atau booking invalid Anda dapat menolak konfirmasi pembayaran
                              </div>
                              <div class="col-md-6">
                                <form action="" method="post" onsubmit="return confirm('Yakin konfirmasi pembayaran?');">
                                <table class="table table-striped">
                                  <tr>
                                    <td colspan="2" style="text-align:center"><h3>Detail Pembayaran</h3></td>
                                  </tr>
                                  <tr>
                                    <td>Bank Pengirim</td>
                                    <td><b><?php echo $row->bank; ?></b></td>
                                  </tr>
                                  <tr>
                                    <td>Atas Nama</td>
                                    <td><b><?php echo $row->pengirim; ?></b></td>
                                  </tr>
                                  <tr>
                                    <td>Total Biaya</td>
                                    <td><b><?php echo rp($row->harga); ?></b></td>
                                  </tr>
                                  <tr>
                                    <td>Jumlah Bayar/DP</td>
                                    <td><input type="text" name="jumlah" class="form-control int" value="<?php echo $row->dp; ?>" placeholder="Jumlah Transfer" data-a-sign="Rp " data-a-dec="," data-a-sep="." required <?php if($status=="Booking Selesai"){echo 'disabled';} ?>></b></td>
                                  </tr>
                                  <tr>
                                    <td>Sisa Pembayaran</td>
                                    <td><b><?php echo rp($sisa); ?></b></td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">*Jika jumlah pembayaran tidak sesuai dengan foto bukti Anda dapat mengubahnya jumlahnya, sisa pembayaran akan terupdate otomatis</td>
                                  </tr>
                                  <?php if($disable==true){ ?>
                                  <tr>
                                    <td colspan="2"><span class="label <?php echo $color; ?>" style="display:inline-block;width:100%"><?php echo $status; ?></span></td>
                                  </tr>
                                  <?php } ?>
                                </table>
                                <input type="hidden" name="idv" value="<?php echo $row->id_inv; ?>">
                                <button type="submit" name="confirmBook" value="confirmBook" class="btn btn-sm btn-block btn-primary" <?php if($disable==true){echo 'disabled';} ?>><i class="fa fa-check"></i> Konfirmasi</button>
                                </form>
                                <div style="padding-top:10px"></div>
                                <form action="" method="post" onsubmit="return confirm('Yakin tolak konfirmasi?');">
                                  <input type="hidden" name="idv" value="<?php echo $row->id_inv; ?>">
                                  <button type="submit" name="cancelBook" value="cancelBook" class="btn btn-sm btn-block btn-danger" <?php if($disable==true){echo 'disabled';} ?>><i class="fa fa-times"></i> Tolak</button>
                                </form>
                                
                                <div style="padding-top:10px"></div>
                                <form action="" method="post" onsubmit="return confirm('Yakin selesaikan book?');">
                                  <input type="hidden" name="idv" value="<?php echo $row->id_inv; ?>">
                                  <input type="hidden" name="jml" value="<?php echo $row->harga; ?>">
                                  <button type="submit" name="selesaiBook" value="selesaiBook" class="btn btn-sm btn-block btn-success" <?php if($status!="Konfirmasi Berhasil"){echo 'disabled';} ?>><i class="fa fa-check-circle"></i> Selesai</button>
                                  <?php if($status=="Konfirmasi Berhasil"){echo '**Jika booking selesai status pembayaran akan menjadi lunas.';} ?>
                                </form>

                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    
                  </td>
                  <td><br>
                    <ul>
                      <?php
                      $no = 0;
                      foreach($json as $obj){
                        if($obj->id_inv == $row->id_inv){
                          $no++;
                          $tgl = date('d/m/Y', strtotime($obj->book_date));
                          $jam = json_decode($obj->book_time);
                          $first = $jam[0];
                          $last = end($jam);
                          echo "<li>$no. $tgl / Lapangan $obj->lapangan / $first - $last</li>";
                        }
                      }
                      ?>
                      <!-- <li>17-05-2018 / Lapangan 2 / 20.00 - 21.00</li> -->
                    </ul>
                  </td>
                </tr>
                <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
