 <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Booking Lapangan
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="row">
           <!-- general form elements -->
          <div class="box box-success">
            <!-- /.box-header -->
            <!-- form start -->
            <div class="row">
            <div class="col-lg-6">
                <img src="<?php echo base_url("assets/dist/img/futsal1.jpeg");?>" class="img-responsive" alt="User Image">
            </div>
            <div class="col-lg-6">
            <form role="form" method="post" action="">
              <div class="box-body">
                <div class="form-group">
                  <label>Lapangan</label>
                  <select class="form-control" name="lap" id="lap" onChange="resetAll();" required>
                    <option value="" selected disabled>-- Pilih Lapangan --</option>
                    <option value="1">Lapangan 1</option>
                    <option value="2">Lapangan 2</option>
                    <option value="3">Lapangan 3</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Pilih Tanggal Main</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" placeholder="Tanggal Main" class="form-control pull-right" name="tgl" id="tgl" onChange="getJam(this.value); resetWkt();" autocomplete="off" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Pilih Jam Main</label>
                  <select class="form-control" name="jam" id="jam" onChange="getWkt(this.value);" required>
                    <option value="" disabled selected>-- Pilih Jam --</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Durasi Bermain</label>
                  <select class="form-control" name="wkt" id="wkt" required>
                    <option value="" disabled selected>-- Pilih Durasi --</option>
                  </select>
                </div>
                
                <button type="submit" name="add" value="add" class="btn btn-primary">Tambah</button>
              </div>  
              </div>
              </div>
              <!-- /.box-body -->
            </form>
          </div>
          <!-- /.box --> 
        </div>
        <div class="row">
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">Bookingan Anda</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <table class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Lapangan</th>
                  <th>Tanggal Main</th>
                  <th>Jam Main</th>
                  <th>Durasi Main</th>
                  <th>Subtotal</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                    $total = 0;
                    $harga = $harga->value;
                    if(isset($_SESSION['cart'])){
                      $cart = $_SESSION['cart'];
                      foreach ($cart as $booking){?>
                        <tr>
                          <td>Lapangan <?php echo $booking['Lapangan']; ?></td>
                          <td><?php echo $booking['Tanggal']; ?></td>
                          <td>
                            <?php
                              $json = json_decode($booking['Jam']);
                              $first = $json[0];
                              $last = end($json);
                              echo "$first - $last";
                            ?>
                          </td>
                          <td><?php echo $booking['Waktu']; ?> Jam</td>
                          <td>
                            <?php
                              $duration = $booking['Waktu'];
                              $subtotal = $harga * $duration;
                              $total = $total + $subtotal;
                              echo $subtotal;
                            ?>
                          </td>
                        </tr>
                  <?php }}else{ ?>
                    <tr>
                      <th colspan=6 style="text-align:center"><h3>Data Booking Kosong</h3></th>
                    </tr>
                  <?php } ?>
                  <tr>
                    <th colspan="4" style="text-align:right">Jumlah Lapangan: </th>
                    <th><?php echo $this->session->userdata('qty'); ?></th>
                  </tr>
                  <tr>
                    <th colspan="4" style="text-align:right">Total Harga: </th>
                    <th><?php echo $total; ?></th>
                  </tr>
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <div class="pull-right">                  
                <form action="" method="post" style="display: inline-block;" onsubmit="return confirm('Hapus semua data pesanan?');"><button type="submit" name="clear" value="clear" class="btn btn-danger"><i class="fa fa-trash"></i> Bersihkan Pesanan</button></form>
                <form action="" method="post" style="display: inline-block;" onsubmit="return confirm('Yakin semua pesanan sudah benar?');">
                <input type="hidden" name="total" value="<?php echo $total; ?>">
                <button type="submit" name="saveBook" value="saveBook" class="btn btn-success"><i class="fa fa-check"></i> Simpan Pesanan</button>
                </form>
              </div>
            </div>
          </div>
          <!-- /.box -->
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->