 <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Konfirmasi Pembayaran
        </h1>
      </section>
      <!-- Main content -->
      <section class="content">
        <div class="row">
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-success">
            <!-- form start -->
            <form action="" method="post" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label>No Invoice</label>
                  <input type="hidden" name="idv" value="<?php echo $idv ?>">
                  <input type="text" class="form-control" value="#<?php echo $idv; ?>" disabled>
                </div>
                <div class="form-group">
                  <label>Bank Pengirim</label>
                  <input type="text" name="bank" class="form-control" placeholder="Bank Pengirim" required>
                </div>
                <div class="form-group">
                  <label>Atas Nama</label>
                  <input type="text" name="nama" class="form-control" placeholder="Atas Nama" required>
                </div>
                <div class="form-group">
                  <label>Jumlah Transfer</label>
                  <input id="jumlah" type="text" name="jumlah" class="form-control" value="<?php echo $nml; ?>" placeholder="Jumlah Transfer" data-a-sign="Rp " data-a-dec="," data-a-sep="." required>
                </div>
                <div class="form-group">
                  <label>Upload Bukti Pembayaran</label>
                  <input type="file" name="proof" class="form-control" required>
                  <small>*Pastikan gambar terlihat dengan jelas</small>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" name="saveConfirm" value="saveConfirm" class="btn btn-primary">Submit</button>
                <a href="<?php echo base_url('user/panel.html'); ?>" class="btn btn-danger">Cancel</a>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
      </div>
      <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->