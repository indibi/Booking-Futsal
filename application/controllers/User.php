<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
        parent::__construct();
		$this->load->model('M_user');
		$this->load->model('M_global');
		$halaman = $this->uri->segment(2);
		if ( !in_array($halaman, array('login','register'), true ) ) {
            if($this->session->userdata('LOGIN')!=TRUE){
				$this->session->set_flashdata('type','alert-warning');
                $this->session->set_flashdata('notif','Harap login terlebih dahulu sebelum memesan.');
                redirect('auth/login');
			}
			if($this->session->userdata('LEVEL')!=1){
				redirect('admin/panel');
			}
		}
		
	}
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		redirect('user/panel');
	}
	public function panel(){

		if($this->input->post('cancelBook')){
			$idv = $this->input->post('idv');
			$dataI = array(
				'status' => 'Booking Dibatalkan'
			);
			$dataB = array(
				'status' => 0
			);
			$where = array(
				'id_inv' => $idv
			);
			$delInv = $this->M_global->update('invoice',$dataI,$where);
			$delBook = $this->M_global->update('book',$dataB,$where);
			echo "<script>alert('Booking telah dibatalkan.')</script>";
			echo "<script> window.location.href = ''</script>";
		}

		$data['peraturan'] = $this->M_global->confGet('peraturan');

		$data['pesanan'] = $this->M_user->getInvoiceAll();

		$this->load->view('skeleton_front/skeleton_header');
		$this->load->view('skeleton_front/skeleton_navbar');
		$this->load->view('front/v_index',$data);
		$this->load->view('skeleton_front/skeleton_footer');
	}
	public function booking(){

		if($this->input->post('clear')){
			unset(
				$_SESSION['qty'],
				$_SESSION["cart"],
				$_SESSION['keranjang']
			);
			redirect('user/booking');
		}

		if($this->input->post('saveBook')){
			if(isset($_SESSION["cart"])){
				$data = array(
					'harga' => $this->input->post('total'),
					'id_user' => $this->session->userdata('ID')
				);
				$id_inv = $save = $this->M_global->insert('invoice',$data);

				$e=count($_SESSION['cart']);
				$i=1;
				$json = json_encode($_SESSION["cart"]);
				$json = json_decode($json, true);
				while($i <= $e){
					$lapangan = $json[$i]['Lapangan'];
					$jam = $json[$i]['Jam'];
					$tgl =  $json[$i]['Tanggal'];
					$waktu = $json[$i]['Waktu'];
					$i++;

					$tanggal = date("Y-m-d", strtotime($tgl));

					$data = array(
						'lapangan' => $lapangan,
						'book_date' => $tanggal,
						'book_time' => $jam,
						'hour_length' => $waktu,
						'id_user' => $this->session->userdata('ID'),
						'id_inv' => $id_inv
					);
					$save = $this->M_global->insert('book',$data);
				}
				unset(
					$_SESSION['qty'],
					$_SESSION["cart"],
					$_SESSION['keranjang']
				);
				echo "<script>alert('Terimakasih, pesanan Anda telah kamis simpan dengan nomor invoice #$id_inv yang dapat dilihat pada panel Anda, silahkan segera membayarkan DP minimal 20% dari harga total.')</script>";
				echo "<script> window.location.href = 'panel.html'</script>";
			}
		}

		if($this->input->post('add')){
			if(isset($_SESSION['cart'])){
				$b = $_SESSION['cart'];
				if(array_search($this->input->post('lap'), array_column($b, 'Lapangan')) !== False) {
					echo "<script>alert('Anda tidak dapat booking lapangan yang sama dalam satu invoice!')</script>";
					echo "<script> window.location.href = ''</script>";
					die();
				}
			}

			$id_user = $this->session->userdata('ID');
			if($id_user=='' OR $id_user==null){
				$sess_array = array(
					'ID'    => '',
					'EMAIL' => '',
					'NAMA'  => '',
					'LEVEL' => '',
					'LOGIN' => FALSE
				);

				$this->session->unset_userdata($sess_array);
				session_destroy();
				redirect('user/login');
			}
			
			$lap = $this->input->post('lap');
			$tgl = $this->input->post('tgl');
			$jam = $this->input->post('jam');
			$wkt = $this->input->post('wkt');

			$wktx = $wkt * 2;
			
			$tanggal = date("Y-m-d", strtotime($tgl));

			$start = strtotime($jam);
			$range = array();
			$range[] = date('H:i', $start);
			for($i=1; $i<=$wktx;$i++){
				$start = strtotime('+30 minutes',$start);
				$range[] = date('H:i', $start);
			}

			$json = json_encode($range);

			$data = array('Lapangan'=>$lap,"Tanggal"=>$tgl,"Jam"=>$json,"Waktu"=>$wkt);

			if(!isset($_SESSION['keranjang'])){
				$no = 1;
				$this->session->set_userdata('keranjang', TRUE);
				$this->session->set_userdata('qty', $no);
				$_SESSION['cart'][$no] = $data;
				redirect('user/booking');
			}else{
				$no = $this->session->userdata('qty');
				$no++;
				$this->session->set_userdata('qty', $no);
				$_SESSION['cart'][$no] = $data;
				redirect('user/booking');
			}
		}

		$data['harga'] = $this->M_global->confGet('harga');

		$this->load->view('skeleton_front/skeleton_header');
		$this->load->view('skeleton_front/skeleton_navbar');
		$this->load->view('front/v_booking',$data);
		$this->load->view('skeleton_front/skeleton_footer');
	}
	public function invoice($idv){

		$whereU = Array(
			'id_user' => $this->session->userdata('ID')
		);

		$whereI = Array(
			'id_inv' => $idv,
			'id_user' => $this->session->userdata('ID')
		);

		$whereB = Array(
			'id_inv' => $idv,
			'id_user' => $this->session->userdata('ID')
		);

		$data['bank'] = $this->M_global->confGet('bank');
		$data['rekening'] = $this->M_global->confGet('rekening');
		$data['nama_rek'] = $this->M_global->confGet('nama_rek');

		$data['user'] = $this->M_global->getOne('user',$whereU);
		$data['inv'] = $this->M_global->getOne('invoice',$whereI);
		$data['book'] = $this->M_global->get('book',$whereB);

		$data['harga'] = $this->M_global->confGet('harga');

		$this->load->view('skeleton_front/skeleton_header');
		$this->load->view('skeleton_front/skeleton_navbar');
		$this->load->view('front/v_invoice',$data);
		$this->load->view('skeleton_front/skeleton_footer');
	}
	public function konfirmasi(){

		if($this->input->post('saveConfirm')){
			$idv = $this->input->post('idv');
			$nama = $this->input->post('nama');
			$bank = $this->input->post('bank');
			$str = $this->input->post('jumlah');
			$str = substr($str, 0, -3);
			$jumlah = preg_replace('/[^0-9]/', '', $str);
			//Move image to upload
			$ext = explode(".", $_FILES["proof"]["name"]);
			$picName = round(microtime(true)) . '.' . end($ext);

			$config = array(
			'upload_path' => FCPATH."/assets/upload/",
			'allowed_types' => "gif|jpg|png|jpeg",
			'overwrite' => TRUE,
			'max_size' => "20480000000000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'file_name' => $picName
			);
			$this->load->library('upload', $config);
			$this->upload->do_upload('proof');

			$dataP = array(
				'bank' => $bank,
				'nama' => $nama,
				'jumlah' => $jumlah,
				'foto_bukti' => $picName,
				'id_inv' => $idv
			);

			$dataI = array(
				'dp' => $jumlah,
				'status' => 'Proses Konfirmasi'
			);

			$where = array(
				'id_inv' => $idv
			);

			$inputPayment = $this->M_global->insert('payment',$dataP);

			$updateInvoice = $this->M_global->update('invoice',$dataI,$where);
			echo "<script>alert('Terimakasih, konfirmasi Anda akan segera di proses.')</script>";
			echo "<script> window.location.href = ''</script>";
			die();
		}

		if($this->input->post('confirm')){

			$data['idv'] = $this->input->post('idv');
			$data['nml'] = $this->input->post('nml');

			$this->load->view('skeleton_front/skeleton_header');
			$this->load->view('skeleton_front/skeleton_navbar');
			$this->load->view('front/v_confirmbook',$data);
			$this->load->view('skeleton_front/skeleton_footer');
		}else{
			redirect('user/panel');
		}
	}
	public function profile(){

		if($this->input->post('chProfile')){
			$table = 'user';
			$where = Array(
				'id_user' => $this->session->userdata('ID')
			);
			$update = Array(
				'nama' => $this->input->post('nama'),
				'alamat' => $this->input->post('alamat'),
				'hp' => $this->input->post('hp')
			);
			$data['update'] = $this->M_global->update($table,$update,$where);
			$this->session->set_userdata('NAMA', $this->input->post('nama'));
			echo "<script>alert('Data Anda berhasil diperbarui.')</script>";
		}

		if($this->input->post('chPw')){

			$data = array(
                'email' => $this->session->userdata('EMAIL'),
                'password' => $this->input->post('opassword')
            );
			$data['chkPw'] = $this->M_user->login($data);

			if($data['chkPw'] != false){
				$where = Array(
					'id_user' => $this->session->userdata('ID')
				);

				$options = [
					'cost' => 11
				];
				$hash = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);
				
				$update = Array(
					'password' => $hash
				);
				$data['update'] = $this->M_global->update('user',$update,$where);
				echo "<script>alert('Password Anda berhasil diperbarui.')</script>";
			}else{
				echo "<script>alert('Password Anda saat ini salah!')</script>";
				echo "<script>alert('Password Anda gagal diperbarui!')</script>";
			}

		}

		$whereU = Array(
			'id_user' => $this->session->userdata('ID')
		);

		$data['user'] = $this->M_global->getOne('user',$whereU);
		
		$data['jmlBook'] = $this->M_global->count('COUNT(*) as jml','invoice',$whereU);

		$data['favLap'] = $this->M_global->favLap($this->session->userdata('ID'));

		$this->load->view('skeleton_front/skeleton_header');
		$this->load->view('skeleton_front/skeleton_navbar');
		$this->load->view('front/v_profile',$data);
		$this->load->view('skeleton_front/skeleton_footer');
	}
}